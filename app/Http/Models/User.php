<?php

namespace App\HTTP\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    //
    protected $table = 'users';

    // 全部许可
    protected $guarded = [];
}
