<?php

namespace App\Http\Controllers;
use App\Http\Models\User;
use Illuminate\Http\Request;

class DemoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        // 模型
        $user = User::get();
        // // 得到传过来的值
        $name = $request -> post('name');
        $phone = $request -> post('phone');
        // // 数据库查找
        $newuser = User::where([
         ['name', '=', $name]
        ]) -> limit(1) -> get();
        // // 是否已存在
        $data['status'] = true;
        if($newuser -> isEmpty()) {
            // 不存在 可加入
            $data['message'] = '提交成功';
            User::create(\Request::all());
            return response() -> json($data);
        } else {
            $data['message'] = '该用户已存在';
            return response() -> json($data);
        };
    }

    public function list(Request $request)
    {   
        // 模型
        $user = User::get();
        // // 是否已存在
        $data['status'] = true;
        if($user -> isEmpty()) {
            // 为空
            $data['message'] = '暂无数据';
            return response() -> json($data);
        } else {
            return response() -> json($user);
        };
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
